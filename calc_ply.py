tokens = ('VARIABLE','NUMBER','PLUS','MINUS','TIMES','DIV','EQUAL','LP','RP') # Token's Tuple #LP = Left Parentheses #RP = Right Parentheses
t_PLUS   = r'\+'
t_MINUS  = r'\-'
t_TIMES  = r'\*'
t_DIV    = r'\/'
t_EQUAL  = r'\='
t_LP = r'\('
t_RP = r'\)'
t_VARIABLE   = r'[_a-zA-Z][_a-zA-Z\d]*'
t_ignore = " \t"
def t_error(t):
    print("illigal Character Entered : {0}".format(t.value[0]))
    t.lexer.skip(1)
def t_NUMBER(t):
    r'\d+'
    t.value = int(t.value)
    return t

import ply.lex as lex
lexer = lex.lex()
############Grammer##############
# S -> E
# E -> E + T | E - T | T
# T -> T * F | T / F | F
# F -> NUMBER | (E) #NUMBER

def p_S__E(p): #S -> E
    'S : E'
    print(p[1])
def p_E__E_plus_T(p): # E -> E + T
    'E : E PLUS T'
    p[0] = p[1] + p[3]
def p_E__E_Minus_T(p): # E -> E - T
    'E : E MINUS T'
    p[0] = p[1] - p[3]
def p_E__T(p): # E -> T
    'E : T'
    p[0] = p[1]
def p_T__T_TIMES_F(p): # T -> T * F
    'T : T TIMES F'
    p[0] = p[1] * p[3]
def p_T__T_DIV_F(p): # T -> T / F
    'T : T DIV F'
    p[0] = p[1] / p[3]
def p_T__F(p): # T -> F
    'T : F'
    p[0] = p[1]
def p_F__NUMBER(p): # F -> NUMBER
    'F : NUMBER'
    p[0] = p[1]
def p_F__LP_E_RP(p): # F -> (E)
    'F : LP E RP'
    p[0] = p[2]

variables = {}
def p_VARIABLE_assignment(p):
    'S : VARIABLE EQUAL E'
    variables[p[1]] = p[3]



def p_print(p):
    'E : VARIABLE'
    try:
        p[0] = variables[p[1]]
    except LookupError:
        print("Meghdare In Reshte Az Ghabl Taiin Nashode Ast!! {0}".format(p[1]))
        p[0] = 0

def p_error(p):
    print("Syntax Error {0}".format(p.value))
import ply.yacc as yacc
parser = yacc.yacc()
while True:
    try:
        input_f = input('Input > ')
    except EOFError:
        break
    parser.parse(input_f)