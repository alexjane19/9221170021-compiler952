l1 = []
flags = {'CF': 0, 'ZF': 0, 'SF': 0, 'OF': 0}
codes = []
variable = {}
label = {}


def status_flags(value):
    if value == 0:
        flags['ZF'] = 1
    if value != 0:
        flags['ZF'] = 0
    if value < 0:
        flags['SF'] = 1
    if value > 0:
        flags['SF'] = 0


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False


def is_variable(s):
    if s.isalpha() and len(s) <= 4:
        return True
    return False

with open('input.asm.txt', 'r', encoding="utf-8") as f1:
    l1 = f1.readlines()
    pc= 0;
for m1 in l1:
    p1 = m1.strip().split()
    if len(p1) > 0:
        codes.append(p1)
        if p1[0][-1] == ':':
            lt = p1[0][:-1]
            if lt[0].isalpha() and lt[1:].isdigit() and len(lt)<=4:
                label.update({p1[0][:-1]: pc});
            else:
                print("syntax error: label definition is error. " + lt)
        pc+=1

pc=0
while(pc< len(codes)):
    m1 = codes[pc]
    print("pc: " , pc)
    print(m1)
    print(label)
    print(variable)
    print(flags)
    pc+=1
    for i in range(len(m1)):

        if m1[i][-1] == ':':
            continue;
        elif m1[i] == 'LDA':
            if is_variable(m1[i+1]):
                variable.update({m1[i+1]: m1[i+2]})
            else:
                print("syntax error: variable definition is error in line " + str(pc) + ". " + m1[i+1])
                pc = len(codes)
            break;

        elif m1[i] == 'MOV':
            if is_variable(m1[i+1]) and is_variable(m1[i+2]):
                if m1[i+2] in variable.keys():
                    variable.update({m1[i+1]: variable[m1[i+2]]})
                else:
                    if is_number(m1[i+2]):
                        variable.update({m1[i + 1]: m1[i + 2]})
                    else:
                        print(m1[i+2]  +" variable is not defined")
            else:
                print("syntax error: variable definition is error in line " + str(pc) + ". " + m1[i+1] + " or " + m1[i+2])
                pc = len(codes)
            break;

        elif m1[i] == 'ADD':
            if is_variable(m1[i + 1]) and is_variable(m1[i + 2]) and is_variable(m1[i + 3]):
                if m1[i+2] in variable.keys() and m1[i+3] in variable.keys():
                    s = int(variable[m1[i+2]]) + int(variable[m1[i+3]])
                    variable.update({m1[i+1] : s})
                    status_flags(s)
                else:
                    print(m1[i+2] + " or " + m1[i+3] + " variable is not defined")
            else:
                print("syntax error: variable definition is error in line " + str(pc) + ". " + m1[i+1] + " or " + m1[i+2]+ " or " + m1[i+3])
                pc = len(codes)
            break;

        elif m1[i] == 'MUL':
            if is_variable(m1[i + 1]) and is_variable(m1[i + 2]) and is_variable(m1[i + 3]):
                if m1[i+2] in variable.keys() and m1[i+3] in variable.keys():
                    s = int(variable[m1[i+2]]) * int(variable[m1[i+3]])
                    variable.update({m1[i+1] : s})
                    status_flags(s)
                else:
                    print(m1[i+2] + " or " + m1[i+3]  +" variable is not defined")
            else:
                print("syntax error: variable definition is error in line " + str(pc) + ". " + m1[i+1] + " or " + m1[i+2]+ " or " + m1[i+3])
                pc = len(codes)
            break;

        elif m1[i] == 'SUB':
            if is_variable(m1[i + 1]) and is_variable(m1[i + 2]) and is_variable(m1[i + 3]):
                if m1[i+2] in variable.keys() and m1[i+3] in variable.keys():
                    s = int(variable[m1[i+2]]) - int(variable[m1[i+3]])
                    variable.update({m1[i+1] : s})
                    status_flags(s)
                else:
                    print(m1[i+2] + " or " + m1[i+3] + " variable is not defined")
            else:
                print("syntax error: variable definition is error in line " + str(pc) + ". " + m1[i+1] + " or " + m1[i+2]+ " or " + m1[i+3])
                pc = len(codes)
            break;

        elif m1[i] == 'JNZ':
            if flags['ZF'] == 0:
                if m1[i + 1] in label.keys():
                    pc = label[m1[i+1]]
                else:
                    print(m1[i+1] + " label is not defined")
            break;

        elif m1[i] == 'JMP':
            if m1[i + 1] in label.keys():
                pc = label[m1[i + 1]]
            else:
                print(m1[i + 1] + " label is not defined")
            break;

        elif m1[i] == 'JZ':
            if flags['ZF'] == 1:
                if m1[i + 1] in label.keys():
                    pc = label[m1[i+1]]
                else:
                    print(m1[i+1] + " label is not defined")
            break;

        elif m1[i] == 'HLT':
            pc = len(codes)
            break;

