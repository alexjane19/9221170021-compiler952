class lexicalAnalyzer:
    def __init__(self, inputFileName='input.calc.txt'):
        self.inputFileName = inputFileName
        self.l1 = ''
        with open(self.inputFileName, 'r', encoding="utf-8") as f1:
            self.l1 = f1.read();
        self.currentToken = {}
        self.index = 0;
        self.start = self.index
        self.lengthOfInput = len(self.l1)
        self.lineNumber = 0;

    def getToken(self):
        self.currentToken = {}
        if self.index == self.lengthOfInput:
            self.currentToken['type'] = 'EOF'
            return self.currentToken;
        while self.l1[self.index] == ' ' or self.l1[self.index] == '\t':
            self.index += 1
        self.start = self.index
        if 'a' <= self.l1[self.index] <= 'z' or 'A' <= self.l1[self.index] <= 'Z' or self.l1[self.index] == '_':
            self.index += 1
            while 'a' <= self.l1[self.index] <= 'z' or 'A' <= self.l1[self.index] <= 'Z' or '0' <= self.l1[self.index] <= '9' or self.l1[self.index] == '_':
                self.index += 1

            if self.l1[self.index] == ':':
                self.currentToken['type'] = 'label'
                self.currentToken['value'] = self.l1[self.start:self.index]
                self.currentToken['row'] = self.lineNumber
                self.currentToken['col'] = self.index
                self.index += 1


            elif self.l1[self.index] == ',':
                self.currentToken['type'] = 'variable'
                self.currentToken['value'] = self.l1[self.start:self.index]
                self.currentToken['row'] = self.lineNumber
                self.currentToken['col'] = self.index
                self.index += 1

            elif self.l1[self.index] == ' ':
                self.currentToken['type'] = 'operator'
                self.currentToken['value'] = self.l1[self.start:self.index]
                self.currentToken['row'] = self.lineNumber
                self.currentToken['col'] = self.index

            elif self.l1[self.start:self.index] == 'print':
                self.currentToken['type'] = 'print'
                self.currentToken['value'] = self.l1[self.start:self.index]
                self.currentToken['row'] = self.lineNumber
                self.currentToken['col'] = self.index
                self.index += 1

            else:
                self.currentToken['type'] = 'variable'
                self.currentToken['value'] = self.l1[self.start:self.index]
                self.currentToken['row'] = self.lineNumber
                self.currentToken['col'] = self.index



        elif '0' <= self.l1[self.index] <= '9':
            isFloat = 0
            while '0' <= self.l1[self.index] <= '9' or self.l1[self.index] == '.':
                if self.l1[self.index] == '.':
                    isFloat +=1
                    if isFloat == 2:
                        self.currentToken['type'] = 'ERROR'
                        self.currentToken['row'] = self.lineNumber
                        self.currentToken['col'] = self.index
                        self.currentToken['msg'] = "syntax error '" + self.l1[self.index] + "'"
                        isFloat += 1


                self.index += 1
            if isFloat == 1:
                n1 = float(self.l1[self.start:self.index])
                self.currentToken['type'] = 'real'
                self.currentToken['value'] = n1
                self.currentToken['row'] = self.lineNumber
                self.currentToken['col'] = self.index
            elif isFloat == 0:
                n1 = int(self.l1[self.start:self.index])
                self.currentToken['type'] = 'int'
                self.currentToken['value'] = n1
                self.currentToken['row'] = self.lineNumber
                self.currentToken['col'] = self.index

        elif self.l1[self.index] == '/' and self.l1[self.index+1] == '/':
            self.start = self.index + 2
            while self.l1[self.index] != '\n':
                self.index += 1

            self.currentToken['type'] = 'comment'
            self.currentToken['value'] = self.l1[self.start:self.index]
            self.currentToken['row'] = self.lineNumber
            self.currentToken['col'] = self.index

        elif self.l1[self.index] == '+':
            self.currentToken['type'] = '+'
            self.index += 1
            self.lineNumber += 1
        elif self.l1[self.index] == '-':
            self.currentToken['type'] = '-'
            self.index += 1
            self.lineNumber += 1
        elif self.l1[self.index] == '*':
            self.currentToken['type'] = '*'
            self.index += 1
            self.lineNumber += 1
        elif self.l1[self.index] == '/':
            self.currentToken['type'] = '/'
            self.index += 1
            self.lineNumber += 1
        elif self.l1[self.index] == '=':
            self.currentToken['type'] = '='
            self.index += 1
            self.lineNumber += 1
        elif self.l1[self.index] == '(':
            self.currentToken['type'] = '('
            self.index += 1
            self.lineNumber += 1
        elif self.l1[self.index] == ')':
            self.currentToken['type'] = ')'
            self.index += 1
            self.lineNumber += 1

        elif self.l1[self.index] == '\n':
            self.currentToken['type'] = 'newline'
            self.index += 1
            self.lineNumber += 1
        else:
            self.currentToken['type'] = 'ERROR'
            self.currentToken['row'] = self.lineNumber
            self.currentToken['col'] = self.index
            self.currentToken['msg'] = "syntax error '" + self.l1[self.index] + "'"

            self.index += 1
        self.start = self.index
        return self.currentToken
lex1 = lexicalAnalyzer()

varVal = {}
var = []
def S():
    t = lex1.getToken();
    A()
    # print("S(): ", t)
    if t['type'] == 'EOF':
        print(varVal)
    else:
        S()

def A():
    if lex1.currentToken['type'] == 'variable':
        var.append(lex1.currentToken['value'])
        t1 = lex1.getToken();
        if t1['type'] == '=':
            x = var.pop()
            var.append(x)
            lex1.getToken();
            varVal[x] = E()

    elif lex1.currentToken['type'] == 'print':
        print(varVal[lex1.getToken()['value']])
        lex1.getToken()
        lex1.getToken()

    elif lex1.currentToken['type'] != 'EOF':
        E()




def E():
    retVal = T();
    t = lex1.currentToken;
    # print("E(): " , t);
    if t['type'] == '+':
        lex1.getToken();
        retVal += E()
    elif t['type'] == '-':
        lex1.getToken();
        retVal -= E()
    return retVal


def T():
    retVal = F();
    t = lex1.getToken();
    # print("T(): " , t);
    if t['type'] == '*':
        lex1.getToken();
        retVal *= T()
    elif t['type'] == '/':
        lex1.getToken();
        retVal /= T()
    return retVal


def F():
    t = lex1.currentToken
    # print("F(): " , t);
    retVal = 2
    if t['type'] == 'int':
        retVal = t['value']
    elif t['type'] == 'real':
        retVal = t['value']
    elif t['type'] == 'variable':
        if t['value'] in varVal.keys():
            retVal = varVal[t['value']]
        else:
            print(t['value'] + " variable is not defined")

    elif t['type'] == '(':
        lex1.getToken()
        retVal = E();
        t1 = lex1.currentToken
        if t1['type'] != ')':      print('Error ) ')
    else:
        print("Unexpected: ", t)
        print('Unexpected token in F');
    return retVal

S()